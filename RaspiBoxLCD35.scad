// My Raspberry Pi 3B enclosure
// -------------------------------------------------------------------
//
// Minimalistic enclosure, may also enclose a 3.5" LCD touchscreen
//
// Optionally, a JST power connector may be used.
// Just because I'm unhappy with the Micro USB power connector 
// and it's position ...
//
// LCD bezel may be glued on LCD frame and the frame itself is bolted
// on bottom shell.
// 
//
// Werner Panocha, December 2020
// -------------------------------------------------------------------

// Updates
// Jan 2021
// Options for external power jack and fan
// Shifted logo on top shell (interferred with fan position)
// - fan sprockets rotated by 30 degrees to match the fan


// Select part(s) to be shown
// -------------------------------------------------------------------

show_board      = 0;           // Show the Raspi PCB
show_assembly   = 1;           // Show assembly of all parts

show_bottom     = 0;           // Show the bottom shell
show_top        = 0;           // Show the top shell
show_lcd_frame  = 0;           // Show frame to mount 3.5" LCD
show_lcd_bezel  = 0;           // Show the LCD bezel

// Select Options:
// -------------------------------------------------------------------
option_sd_slot      = 1;    // Accessible slot for SD card
option_jst_ethernet = 0;    // JST power connector beneath Ethernet
option_jst_sdcard   = 0;    // JST power connector beneath SD card
option_corner_screws = 1;   // Corner screws (with LCD)
option_mount_lugs   = 0;	// Mount lugs

option_ext_power    = 0;    // External power plug
option_fan          = 0;    // Fan

$fn = 72;


// -------------------------------------------------------------------
// Raspberry board// Corner screws (optional)
                
// -------------------------------------------------------------------
// The board code was taken from 
// https://github.com/saarbastler/library.scad
// Not required for the construction of the enclosure itself.
// I just used it for cross checking of the dimensions.
// And I found and fixed a small glitch in the original code ...
// -------------------------------------------------------------------
// >>>
/*
MIT License

Copyright (c) 2017 saarbastler

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// Notice: there was a small glitch with the wrong Y-offset of
// the Ethernet connector

module header(pins, rows)
{
  color("darkgrey") cube([2.54*pins,2.54*rows,1.27]);
  
  color("gold") 
  for(x=[0:pins-1],y=[0:rows-1])
    translate([x*2.54+(1.27+.6)/2,y*2.54+(1.27+.6)/2,-3.5]) cube([0.6,0.6,11.5]);
}

module pi3()
{
  // PCB
  color("blue") difference()
  {
    hull()
    {
      translate([-(85-6)/2,-(56-6)/2,0]) cylinder(r=3, h=1.4 );
      translate([-(85-6)/2, (56-6)/2,0]) cylinder(r=3, h=1.4 );
      translate([ (85-6)/2,-(56-6)/2,0]) cylinder(r=3, h=1.4 );
      translate([ (85-6)/2, (56-6)/2,0]) cylinder(r=3, h=1.4 );
    }
    
    translate([-85/2+3.5,-49/2,-1]) cylinder(d=2.75, h=3);
    translate([-85/2+3.5, 49/2,-1]) cylinder(d=2.75, h=3);
    translate([58-85/2+3.5,-49/2,-1]) cylinder(d=2.75, h=3);
    translate([58-85/2+3.5, 49/2,-1]) cylinder(d=2.75, h=3);
  }
  
  // Header
  translate([3.5-85/2+29-10*2.5,49/2-2.54,1.4])
    header(20,2);
  
  translate([-85/2,-56/2,1.4])  
  {
    color("silver") 
    {
      // Ethernet
      //translate([85-19,11.5-16/2,0]) cube([21,16,13.8]);
      // FIX Y-OFFSET and cube height !!
      translate([85-19,10.25-16/2,0]) cube([21,16,13.5]);
        
      // USB
      translate([85-15, 29-13/2,0]) cube([17,13,15.5]);
      // Added flange!
      translate([85-15+17, 29-14.8/2, 0.5]) cube([.5,14.8,15.5]);
        
      translate([85-15, 47-13/2,0]) cube([17,13,15.5]);
      // Added flange!
      translate([85-15+17, 47-14.8/2, 0.5]) cube([.5,14.8,15.5]);
      
      // micro USB
      translate([10.6-8/2,-1.5,0]) cube([8,6,2.6]);
      
      // HDMI
      translate([32-15/2,-1.5,0]) cube([15,11.5,6.6]);
    }
    
    color("darkgrey") 
    {
      // Audio
      translate([53.5-7/2,-2,0]) 
      {
        translate([0,2,0]) cube([7,13,5.6]);
        translate([7/2,0,5.6/2])rotate([-90,0,0]) cylinder(d=5.6,h=2);
      }
    
      // Display
      translate([1.1,(49-22)/2,0]) cube([4,22,5.5]);
      
      // Camera
      translate([45-4/2,1.1,0]) cube([4,22,5.5]);
    }
    
    // Micro SD Card
    color("silver") translate([0,22,-2.9]) cube([13,14,1.5]);    
    color("darkgrey") translate([-2.4,23.5,-2.65]) cube([2.4,11,1]);
  }
}

// -------------------------------------------------------------------
// <<< End of code take from https://github.com/saarbastler/library.scad
// -------------------------------------------------------------------


// Measurements from official Raspberry drawing
// -------------------------------------------------------------------
// see https://www.raspberrypi.org/documentation/hardware/raspberrypi/mechanical/rpi_MECH_3bplus.pdf

board_x = 85;
board_y = 56;
board_z = 1.5;

mount_x = 58;
mount_y = 49;
mount_o = 3.5;     // hole offset from board corner



// Design values
wall = 3;           // Thickness of walls
gap = 0.5;          // Inner gap wall <--> board


// Outer hull of box
box_x = board_x + 2*wall + 2*gap;
box_y = board_y + 2*wall + 2*gap;

// Add room for external power socket (extra 8 mm should fit)
box_z = (option_ext_power) ? 29 + 8 : 29;


box_r = 2;
box_fw = 1.2;   // fold width
box_fh = 1;     // fold height

// Board position relative to outer hull
brdoff_x = wall + gap;
brdoff_y = brdoff_x;
brdoff_z = wall + 3;               // + standoff


// fan X/Y position (center)
fan_cntr_x = 20;
fan_cntr_y = box_y / 2;



module label(){
    
    text = "Raspberry";
    letterheight = 2;
    lettersize = 8;
    
    linear_extrude(height = letterheight) {
        text("Raspberry", size = lettersize, font = "Arial:style=Bold", halign = "left", valign = "top");
    }
}

// -------------------------------------------------------------------
module standoff(z, di){
    translate([0,0,-.1]){
        difference(){
            union(){
                cylinder(d=10, h=1.1); // enforcement screwhead notch
                cylinder(d=5.5, h=z);
                
            }
            translate([0,0,-.01]) cylinder(d=di, h=z+1);
        }
        
        
          
        // This is an ugly hack ...
        // If we have the corner screws, we do not have screws from bottom.
        // So add some pins to hold the PCB in place
        if(option_corner_screws){
            cylinder(d=3, h= z);
            cylinder(d=2, h= z+1.3);
        }
    }
}

// -------------------------------------------------------------------
module srewhead(sz, dh, di){
    
    cylinder(d=dh, h=sz);
    cylinder(d=di, h=sz + wall);
}


// Mounting lugs
// -------------------------------------------------------------------
module mount_lug(){
	
	difference(){
		union(){
			cylinder(d=8, h=3);
			translate([0,-4,0]) cube([20,8,3]);
		}
		
		translate([0,0,-.1]) cylinder(d=4, h=5);
	}
	
}

module mount_lugs(){
	
	xx = 8;
	yy = 6;
	translate([-xx, yy, 0]) mount_lug();
	translate([box_x + xx, box_y-yy,0]) rotate([0,0,180]) mount_lug();
}



// -------------------------------------------------------------------
// Corner screws (optional)
// -------------------------------------------------------------------
module corner_screw(){
    difference(){
        corner_screw_add();
        corner_screw_cut();
    }
}

module corner_screw_add(){
	
    //cylinder(d=6.5, h=box_z); 
	
	r = 1;
	
	difference(){
		translate([0,0,r])
		minkowski(){
			cylinder(d=6.5 - 2*r, h=box_z); 
			sphere(r);
		}
		
	translate([-5,-5,box_z]) cube([10,10,10]);
	}
	
}

module corner_screw_cut(){
    
    
    if(show_bottom){
		// Bore is smaller for the bottom!
        translate([0,0,1]) cylinder(d=2.5, h=box_z +2);
    }
    else {
        translate([0,0,-1]) cylinder(d=3.0, h=box_z +2);
    }
     
    
    // Screw head
    translate([0,0,box_z-2]) cylinder(d=4.5, h=3);
    *translate([0,0,-1]) cylinder(d=4.5, h=3);   
}


module corner_screws_add(){
    
    // Notice:
    // Option JST Ethernet requires a bigger Y-distance of the screw!
    // To give room for the JST connector
    ix = 3; // .5;
    iy = 0;
    
    if(option_corner_screws){
        translate([ix,iy,0]) corner_screw(); 
        translate([box_x - ix, iy,0]) corner_screw();
        translate([ix,box_y-iy,0]) corner_screw();
        translate([box_x - ix, box_y - iy,0]) corner_screw();
    }
}

module corner_screws_cut(){
    ix = 3; //.5;
    iy = 0;
    
    if(option_corner_screws){
        translate([ix,iy,0]) corner_screw_cut();
        translate([box_x - ix, iy,0]) corner_screw_cut();
        translate([ix,box_y-iy,0]) corner_screw_cut();
        translate([box_x - ix, box_y - iy,0]) corner_screw_cut();
    }
}




// -------------------------------------------------------------------
module topnut(z){
        
    difference(){
        union(){
            cylinder(d=5.5, h=z);
            translate([0,0,5]) {
                hull(){
                    translate([0,-4,5]) cube([5,.1,10], true);
                    cylinder(d=5.5, h=1);
                }
            }
        }
        
     translate([0,0,-.1]) cylinder(d=2.5, h=20);   
    }
}

// -------------------------------------------------------------------
// Outer shell with all breakthroughs
// -------------------------------------------------------------------
// part == "top" or "bottom" shell
// z == Z-offset for the split-line of the 2 shells
module box(part, z){
    
    difference(){
        
        union(){
            
            
            difference(){
                
                union(){
                    // Outer shell (rounded corners)
                    translate([box_r, box_r, box_r])
                    minkowski(){
                        cube([box_x - 2*box_r, 
                              box_y - 2*box_r, 
                              box_z - 2*box_r]);
                        sphere(box_r);
                    }
                    
                    // Corner screws (optional)
                    corner_screws_add();
                }
                
                // Corner screws
                corner_screws_cut();
                
                
                // Inner room ...
                translate([wall, wall, wall])
                cube([box_x - 2*wall, box_y - 2*wall, box_z - 2*wall]);
                
                // Cut shells apart
                
                if(part == "top"){
               
                    // --- TOP ( cut off lower shell)
                    // --------------------------------------
                    translate([-10,-10,-1])
                    cube([box_x + 20, box_y + 20, z+1]);
                }
                else if(part == "bottom"){
                
                    // --- BOTTOM (cut off upper shell)
                    // --------------------------------------
                    
                    // Notice: 
                    // Do it already here, so we can later
                    // add the fold on top of the lower shell afterwards!
                    translate([-10,-10,z])
                    cube([box_x + 20, box_y + 20, box_z]);
                }
            
            } // difference()  slicing box shells
        
            if(part == "top"){
                // Enforcements between USB and Ethernet
                // just easier to print 
                yy = 20.4;
                translate([box_x - 13, 
                       brdoff_y  + 29 - yy/2 -.1, 
                       wall]) //.5 + brdoff_z + board_z -t])
                cube([12, yy, box_z - 2*wall]);
            }
            if(part == "bottom"){
            
                // Add some stuff after cutting out inner room and
                // chopping off the the upper shell.
                
                // Add fold on top of bottom shell
                lo = wall - box_fw + .1;  
                difference(){
                    translate([lo, lo, z-.01])
                    cube([box_x - 2*lo, box_y - 2*lo, box_fh - .1]);
                    
                    translate([wall, wall, z - .1])
                    cube([box_x - 2*wall, box_y - 2*wall, box_fh + 2]); 
                }
                
                
                // Make SD slot smaller 
                // (avoid accidential insert of card into the box ..)
                if(option_sd_slot){
                    translate([wall -.01, (box_y/2) - 9, brdoff_z - 1.8 - wall])
                    cube([2, 18, wall]);
                }
				
				
				// Mounting lugs
				if(option_mount_lugs){
					mount_lugs();
				}
                 
            }
            
            
        }
    
        // Punch out all connector breakthroughs 
        // -------------------------------------------------------------
        
        // Tolerance (==gap for breakthroughs)
        t = 0.5;
        
        
        // --- Ethernet ---------------------------------
        ey = 16.0 + 2*t;
        ez = 13.5 + 2*t;
        translate([box_x - wall - 16, 
                   brdoff_y + 10.25 - ey/2, 
                   brdoff_z + board_z -t])
        cube([30, ey, ez]);
        
        // --- USB --------------------------------------
        uy = 14.8 + 2*t;
        uz = 15.6 + 2*t;
        translate([box_x - wall - 16, 
                   brdoff_y + 47 - uy/2, 
                   .5 + brdoff_z + board_z -t])
        cube([30, uy, uz]);
        
        translate([box_x - wall - 16, 
                   brdoff_y  + 29 - uy/2, 
                   .5 + brdoff_z + board_z -t])
        cube([30, uy, uz]);
        
        // Extra cut in enforcement
        translate([box_x-30-wall, 
                   brdoff_y  + 29 - uy/2, 
                   box_z - uz - wall ])
        cube([30, uy, uz]);
        
        
        // --- Power ------------------------------------
        // punch through
        translate([brdoff_x + 10.6 - (8 + 2 *t)/2,  
                    -.1,
                    brdoff_z + board_z - t - .5]){ 
            cube([8 + 2 *t, 10, 3 + 2*t]);
            if(part == "bottom"){
                // Extra punch to remove the fold on top
                *translate([0,0,box_fh + .1])
                cube([8 + 2 *t, 10, 3 + 2*t]);
            }
        }
                    
        // connector inset
        pi = 2.5;                                               // 1.5
        translate([brdoff_x + 10.6 - (8 + 2 *t +2*pi)/2,  
                    -.8,                                        // -1,
                    brdoff_z + board_z - t - pi -.5]) 
        cube([8 + 2 *t + 2*pi, wall, 3 + 2*t + 2*pi]);
        
        
        // --- HDMI --------------------------------------
        // punch through
        translate([brdoff_x + 32 - 8,  
                    -.1,
                    brdoff_z + board_z - t]) 
        cube([15 + 2*t, 10, 6.5 + 2*t]);
        
        // connector inset
        hi = 1.5;
        translate([brdoff_x + 32 - 8 -hi,  
                    -1,
                    brdoff_z + board_z - hi - t]) 
        cube([16 + 2*hi, wall, 7 + 2*hi]);
        
        
        // --- Audio -------------------------------------
        translate([brdoff_x + 53.5, 
                   wall + 1, 
                   brdoff_z + board_z + 3]){
        
            // punch through
            rotate([90,0,0])  cylinder(d=7, h=10);
            
            // Connector inset
            translate([0,-2.5,0])
            rotate([90,0,0])  cylinder(d=10, h=10);
        }
        
        
        // --- JST Power (optional) ----------------------
            
        // beside SD card
        if(option_jst_sdcard){
            translate([19 +1.5, box_y - 13, brdoff_z - 4.6])
            rotate([0,0,180])
            jst_punch(.2);  // extra room!
            
            
            // room for cable feedthrough
            translate([brdoff_x,  13, brdoff_z + .75])
            rotate([-90,0,0])
            cylinder(d=5, h=5);
        } 
        
        // beside Ethernet
        if(option_jst_ethernet){
            translate([box_x - 21, 5.75, brdoff_z + 1.5])
            rotate([90,0,0])
            jst_punch(.1);    
        }
        
        // --- External Power (optional) -----------------
        
        // leave room for the mounting nut!
        // place connector above the Ethernet connecor
        if(option_ext_power){
            nut_d  = 12.5;
            plug_d = 7.8;
            translate([box_x - 5,                   // punch ..
                      brdoff_y + 10.25,             // ctr of Ethernet conn
                      box_z - (wall + nut_d/2)])    // distance from wall
            rotate([0,90,0])
            difference(){
                cylinder(d=plug_d, h=10);
                
                translate([plug_d - .8 ,0,0])
                cube([plug_d, plug_d, 10],true);
                
            }
        }
        
        // --- Fan (optional) -------------------------
        if(option_fan){
            translate([fan_cntr_x, fan_cntr_y, box_z - wall - 1])
            fan_punch();
            
            fan_exhaust();
        }
        
        
        
        // Cut per lower / upper part  of shell
        // ============================================
        if(part == "top"){
            
            // --- TOP 
            // --------------------------------------
            
            // Cut parts extending to lower shell (e.g. enforcements)
            translate([-1,-1,-1])
                    cube([box_x + 2, box_y + 2, z+1]);
            
            // Cut out fold
            // --------------------------------------
            lo = wall - box_fw - .1;  
            translate([lo, lo, z-.1])
            cube([box_x - 2*lo, box_y - 2*lo, box_fh + .1]);
            
            
        }
        else if(part == "bottom"){
            
            // --- BOTTOM  
            // --------------------------------------
            
            // Cut out SD-Card access
            // --------------------------------------
            if(option_sd_slot){
                translate([-.01, (box_y/2) - 8, -.1])
                cube([4,16,6.5]);
            }
            else {
                // Make room for inserted card !
                translate([.8, (box_y/2) - 8, wall-.01])
                cube([4,16,3.5]);
                
            }
            
            
            // Decide whether we will have screws in bottom or not
            if(!option_corner_screws){
                // Cut out mounting screw heads
                // -------------------------------------- 
                translate([brdoff_x, brdoff_y, -.01]){
                   // Standoffs relative to board
                    translate([mount_o, mount_o, 0]){ 
                        sz = 2.2;     // depth
                        dh = 5.5;   // head
                        di = 3;     // bore
                        translate([0,       0,      0]) srewhead(sz, dh, di);
                        translate([mount_x, 0,      0]) srewhead(sz, dh, di);
                        translate([mount_x, mount_y,0]) srewhead(sz, dh, di);
                        translate([0,       mount_y,0]) srewhead(sz, dh, di);
                    }
                }
            }
            
        } // if(part == bottom)
        
    } // difference() from shell
    
    
    // Add the standoffs
    if(part == "bottom"){
        translate([brdoff_x, brdoff_y, wall]){
    
            // Standoffs relative to board
            translate([mount_o, mount_o, 0]){
                sz = 3; // height
                di = 3; // bore
                translate([0,       0,      0]) standoff(sz, di);
                translate([mount_x, 0,      0]) standoff(sz, di);
                translate([mount_x, mount_y,0]) standoff(sz, di);
                translate([0,       mount_y,0]) standoff(sz, di);
              
            }
        }  
    }
    else {
        translate([brdoff_x, brdoff_y, brdoff_z + board_z +.1]){
    
            // Top nuts for fixing the board
            translate([mount_o, mount_o, 0]){
                sz = 5;
                translate([0,       0,      0]) topnut(sz);
                translate([mount_x, 0,      0]) topnut(sz);
                
                translate([mount_x, mount_y,0]) rotate([0,0,180]) topnut(sz);
                translate([0,       mount_y,0]) rotate([0,0,180]) topnut(sz);
            }
        }
    }
}


// Upper shell for standard box
// -------------------------------------------------------------------
module top_shell(){
    box("top", wall + 3 + board_z + 2 + .1);
    translate([44, box_y/2, box_z - (wall - 0.01)])
    rotate([0,180,0]) small_logo2();
}

// Bottom shell for standard box
// -------------------------------------------------------------------
module bottom_shell(){
    box("bottom", wall + 3 + board_z + 2 -.1);
    translate([40, box_y/2, wall - 0.01])
    rotate([0,0,0]) small_logo2();
}


// Bezel for 3.5" LCD (upper shell)
// -------------------------------------------------------------------
module lcd_bezel(){
    
    lcd_vx = 76;    // view port XY
    lcd_vy = 51.5; //52;
    lcd_z = 6.5;    // thickness of LCD panel 
    
    bezel_z = 1;    // thickness of bezel
    t = 0.2;        // tolerance (gap)
    
    
    difference(){
        union(){
            
            // Wall
            w = 2;

            // Cut part from upper shell
            difference(){
                
                union(){
                    // Solid outer shell (rounded corners)
                    translate([box_r, box_r, box_r])
                    minkowski(){
                        cube([box_x - 2*box_r, 
                              box_y - 2*box_r, 
                              box_z - 2*box_r]);
                        sphere(box_r);
                    }
                    // Corner screws (optional)
                    corner_screws_add();
                }
                
                
                
                // Inner room
                translate([w, w, box_z-(15 + bezel_z)])
                cube([box_x - 2*w, box_y - 2*w, 15]);
                
                // Cut off LCD bezel height
                translate([-10, -10, box_z -25 - (lcd_z + bezel_z + t )])
                cube([box_x + 20, box_y + 20, 25]);
             
                // Cut out view port
                translate([6, 6, box_z - bezel_z -.01])  // old: 6, 5
                hull(){
                    translate([-bezel_z, -bezel_z , 2*bezel_z]) 
                    cube([lcd_vx + 2*bezel_z, lcd_vy + 2*bezel_z, .01]);
                
                    cube([lcd_vx, lcd_vy, .01]);
                }
                
            }
            
           
            // Adjustment pins for bezel mounting (glueing)
            dx = 1.5;
            zx =  box_z - ( bezel_z + lcd_z + t +1);
            hx =  bezel_z + lcd_z + t - 0; //9.9;
            
            translate([2.5, 2.5, zx])
            cylinder(d=dx, h = hx);
            translate([2.5, box_y - 2.5, zx])
            cylinder(d=dx, h = hx);
            translate([box_x - 2.5, 2.5, zx])
            cylinder(d=dx, h = hx);
            translate([box_x - 2.5, box_y - 2.5, zx])
            cylinder(d=dx, h = hx);
            
            
        }
        
        // Corner screws (optional)
        corner_screws_cut();
    }
}


// Frame for 3.5" LCD (upper shell)
// -------------------------------------------------------------------
module lcd_frame(){
    
    difference(){
        union(){
            
            // Cut part from upper shell
            difference(){
             top_shell();
             
             // Cut at LCD level
             translate([-10, -10, brdoff_z + 18])
             cube([box_x + 20, box_y + 20, 10]);
                
            }
            
            // add plane for LCD
            difference(){
                translate([1, 1, brdoff_z + 18 -1])
                cube([box_x - 2, box_y -2, 1]);  // << == align Z with bezel !!!
            
                // main cutout
                translate([brdoff_x + 10, brdoff_y + 5, 17])
                cube([board_x-20, board_y-10, 30]);
                
                // connector cutout
                translate([brdoff_x + 6, brdoff_y + 49, 17])
                cube([35, 7, 30]);
                
                // USB cut
                t = .5;
                uy = 14.8 + 2*t;
                uz = 15.6 + 2*t;
                translate([box_x - wall , 
                           brdoff_y + 47 - uy/2, 
                           .5 + brdoff_z + board_z -t])
                cube([30, uy, uz]);
                
                translate([box_x - wall, 
                   brdoff_y  + 29 - uy/2, 
                   .5 + brdoff_z + board_z -t])
                cube([30, uy, uz]);
                
                corner_screws_cut();
                
            }
        } // union()
        
        // Cut out adjustment holes for bezel mounting (glueing)
        translate([2.5, 2.5, brdoff_z + 18 - 2])
        cylinder(d=2.5, h = 10);
        translate([2.5, box_y - 2.5, brdoff_z + 18 - 2])
        cylinder(d=2.5, h = 10);
        translate([box_x - 2.5, 2.5, brdoff_z + 18 - 2])
        cylinder(d=2.5, h = 10);
        translate([box_x - 2.5, box_y - 2.5, brdoff_z + 18 - 2])
        cylinder(d=2.5, h = 10);
    }
    
    translate([7.4,30,23.01])
    #rotate([0,180,180]) small_logo2();
}


// JST connector punch
// -------------------------------------------------------------------

module jst_punch(t){
    
    // socket body
    translate([8 + 2*t, 0 , 0])  cube([ 11.1 + 2*t, 6.9 + 2*t, 4.2 + 2*t]);
    translate([0, 0.5 , 0])  cube([ 19.1+ 2*t, 5.9+2*t, 4.2+2*t]);
    
    //connector body
    //translate([19, 0.75, 0.5]) cube([ 5 + 2*t, 5.4 +2*t, 3.2 + 2*t]);
    translate([19, 0.75-.3, 0.5]) cube([ 5 + 2*t, 6 +2*t, 3.2 + 2*t]);
}


// Display assembly
// -------------------------------------------------------------------
module display_assy(){
    
    // Top cover with text
    *rotate([-30,0,0]) translate([0,-10,50])
    difference(){
        box("top", wall + 3 + board_z + 3);
        translate([26,0.5,25]) rotate([90,0,0]) label();
    }
    
    // LCD version
    translate([0,0,40])
    rotate([-20,0,0]) translate([0,-10,30]) {
        // LCD  bezel
         translate([0,0,10]) lcd_bezel();
        
        // LCD Frame
         lcd_frame();
	}
    // Plain box
    translate([0,0,10])
    rotate([-20,0,0]) translate([0,-10,20])
    top_shell();
    
    
    
    // Board
    *translate([0,0,10])
    translate([board_x/2 + brdoff_x, board_y/2 + brdoff_y, brdoff_z])
    pi3();

    // Bottom shell
    bottom_shell();
    //box("bottom", wall + 3 + board_z + 3);


}

// Letter 'W' for logo
// -------------------------------------------------------------------
module logo_W(){
 
    x = .4;
    
    translate([0,0,x/2]){   
        rotate([0,0,13]) cube([0.4,3,x], true);
        translate([1.0, 0, 0])    rotate([0,0,-13])  cube([0.4,3,x], true);
        
        translate([2.0,0,0]){
        rotate([0,0,13]) cube([0.4,3,x], true);
        translate([01.0,0,0])    rotate([0,0,-13])   cube([0.4,3,x], true);
        }
    }   
}

// Letter 'P' for logo
// -------------------------------------------------------------------
module logo_P(){
    
    $fn = 72;
    
    x = .4;
    
    translate([0,0,x/2])
    cube([0.4,3,x], true);
    
    translate([0.3, 1.1, 0])
    cube([0.4,0.4,x]);
    
    translate([0.3, -.5, 0])
    cube([0.4,0.4,x]);
    
    translate([.7, .5, 0])
    difference(){
        cylinder(d=2, h=x);
        translate([0,0,-.01])
        cylinder(d=1.2, h=x + .02);
        
        translate([-3,-1.5,-.01])
        cube([3,3,1]);
    }
}


// Logo with ellipse
// -------------------------------------------------------------------
module small_logo(){

    x = .4;
    
	translate([-2.5,0,0]){
		logo_W();
		translate([4,0,0]) logo_P();
	}
	
	
	difference(){
		resize([10,5.5,x])
		cylinder(d=10, h=x);
		
		resize([9.6, 5.1, x*1.1])
		translate([0,0,-.01])
		cylinder(d=9, h=x*1.1);
	}
}

// Logo with triangle
// -------------------------------------------------------------------
module small_logo2(){

    x = .4;
    
	translate([-2.5, 0.7, 0]){
		logo_W();                       // W
		translate([4,-1,0]) logo_P();   // P
	}
	
    rotate([0,0,13]){
        $fn = 3;
        difference(){
            // outer triangle
            cylinder(d=12.5, h=x);
            translate([0,0,-.1])
            cylinder(d=11, h=x+.2);
            
        }
        
        translate([-1.54, -2.7, 0])
        cylinder(d=3, h=x);
    }
}


// Pi fan (30mm) puching pattern
// -------------------------------------------------------------------
module fan_punch(){
    
    ds = 2.8;
    
    // Fan
    difference(){
        cylinder(d=28, h=10);
        
        rotate([0,0,30])
        translate([0,0,-1]){
            cylinder(d=16, h=10+2);
            
            cube([1.2,30,20+2],true);
            rotate([0,0,60])
            cube([1.2,30,20+2],true);
            rotate([0,0,120])
            cube([1.2,30,20+2],true);
        }
    }
    
    // Screws 
    translate([12,12,0])    cylinder(d = ds, h=10);
    translate([-12,-12,0])  cylinder(d = ds, h=10);
    
    // Exhaust
    
  
}

module fan_exhaust(){
    
    zz = 2.6;
    w = 12;
    
    translate([0,0,box_z - wall - zz]){
        translate([box_x - 5, 27, 0])
          cube([w,w,zz]);
        
        translate([box_x - 5, 45, 0])
          cube([w,w,zz]);
    }
    translate([0,0,box_z - wall - zz - 4]){
        translate([box_x - 5, 27, 0])
          cube([w,w,zz]);
        
        translate([box_x - 5, 45, 0])
          cube([w,w,zz]);
    }
    
}

// -------------------------------------------------------------------
// Display / printing
// -------------------------------------------------------------------

// Check with cutout
*difference(){
    union(){
        bottom_shell();
        top_shell();
        
        *translate([board_x/2 + brdoff_x, board_y/2 + brdoff_y, brdoff_z])
        pi3();
    }
    
    translate([-1, -1, -1])
   cube(15);
}



if(show_assembly){
    // Display parts assembly
    display_assy();
}
else {
    
    // Bottom shell
    if(show_bottom)
        bottom_shell();
    
    
    // Top shell
    if(show_top)
        top_shell();
    
    // LCD frame
    if(show_lcd_frame)
        translate([0,0,10]) lcd_frame();
    
    // LCD  bezel
    if(show_lcd_bezel)
         translate([0,0,20]) lcd_bezel();

}

// Board
if(show_board){
    translate([board_x/2 + brdoff_x, board_y/2 + brdoff_y, brdoff_z])
    %pi3();
}




// Garbage ...
// -------------------------------------------------------------------

*intersection(){
 bottom_shell();
translate([0,-3,0])
cube(25);
}

*fan_punch();

*mount_lugs();

*jst_punch(.1);


*small_logo2();


*translate([26,0.5,25]) rotate([90,0,0]) label();
// Outer hull
*%box();

*#cube([20,20,1.5]);
//translate([9,11,0])
*logo_W();

*translate([4,0,0])
logo_P();

*translate([-6, -6, 0])
cube([12,12,1.5]);
*translate([0,0,1.49]) #small_logo();