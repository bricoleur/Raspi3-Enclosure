# Raspberry Pi Enclosure
designed with [OpenSCAD](https://openscad.org/)

Can be customized to some extend ...

* Different power supply connector types
* Mounting lugs
* Access to memory card
* Fan
* Combine with 3.5" Touchscreen

Unfortunately it does not fit to Raspberry Pi 4

See the following section in sourcecode

```
// Select part(s) to be shown 
// -------------------------------------------------------------------

show_board      = 0;           // Show the Raspi PCB
show_assembly   = 0;           // Show assembly of all parts

show_bottom     = 1;           // Show the bottom shell
show_top        = 1;           // Show the top shell
show_lcd_frame  = 0;           // Show frame to mount 3.5" LCD
show_lcd_bezel  = 0;           // Show the LCD bezel
```
Eventually everything may be changed by modifying the source code.  
*OpenSCAD: The Programmers Solid 3D CAD Modeller*

### Component Overview

![Alt text](png/full_assy.png)  

# STL Files

For those who dont like to use OpenSCAD some STL files are provided in the 'stl' folder (watch this beautiful STL viewer in Codeberg!)  
  
Hint: But this viewer seems to have an **issue with STL** files in **ASCII format**.  
*I had a reproduceable effect were a file locked up my PC when clicking on the STL link. It gets so busy that it was almost not responsible and I decided to power off to regain control. File could be viewed in other online viewer without any issue.
Problem in Codeberg disappeared when I used STL export in binary format!  
Unfortunately older OpenSCAD releases generate only ASCII STL (but 2021.01 seems to be fine).*



## Small box

![Alt text](png/small_box_assy.png) 

Requires **[base.stl](stl/base.stl)** and **[small-cover.stl](stl/small-cover.stl)**.


## Cover with Fan and external Power Jack

 
  ![Alt text](png/fancover.png)  

Just use **[fan-cover.stl](stl/fan-cover.stl)**


## 3.5" Touch Screen

![Alt text](png/lcd_assy.png)  

Requires **[lcd_base.stl](stl/lcd_base.stl)**, **[lcd_frame.stl](stl/lcd_frame.stl)** and **[lcd_bezel.stl](stl/lcd_bezel.stl)** (from bottom to top).




